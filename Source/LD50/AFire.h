// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AFire.generated.h"

class UPointLightComponent;
class UFireComponent;

UCLASS()
class LD50_API AAFire : public AActor
{
	GENERATED_BODY()
	
	void UpdateMesh();

public:	
	// Sets default values for this actor's properties
	AAFire();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Components
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BonfireMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UPointLightComponent* Light;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class UParticleSystemComponent* Particles;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UFireComponent* FireComponent;

	UFUNCTION(BlueprintPure)
	float GetPowerRate();

	UFUNCTION(BlueprintCallable)
	void Feed(float value);
	
	UFUNCTION(BlueprintImplementableEvent)
	void AtFeed();

	UFUNCTION(BlueprintImplementableEvent)
	void AtDeath();

	UPROPERTY(EditAnywhere, Category = Fire)
	TArray<UStaticMesh*> BonfireMeshes;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Light)
	float MaxAttenuationRadius = 1000;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Light)
	float MaxIntensity = 5000;
};
