// Fill out your copyright notice in the Description page of Project Settings.


#include "AFire.h"
#include "FireComponent.h"
#include "Components/PointLightComponent.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
AAFire::AAFire()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	this->BonfireMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bonfire Mesh"));
	this->RootComponent = this->BonfireMesh;
	this->Light = CreateDefaultSubobject<UPointLightComponent>(TEXT("LightComp"));
	this->Light->SetupAttachment(RootComponent);
	this->Light->SetRelativeLocation(FVector(0, 0, 40));

	this->Light->SetAttenuationRadius(this->MaxAttenuationRadius);
	this->Light->SetIntensity(this->MaxIntensity);

	this->Particles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FireParticles"));
	this->Particles->SetupAttachment(RootComponent);

	FireComponent = CreateDefaultSubobject<UFireComponent>(TEXT("Fire component"));
	this->UpdateMesh();
}

// Called when the game starts or when spawned
void AAFire::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AAFire::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto powerRate = this->GetPowerRate();

	if (powerRate <= 0) {
		this->AtDeath();
	}

	this->Light->SetAttenuationRadius(this->MaxAttenuationRadius * powerRate);
	this->Light->SetIntensity(this->MaxIntensity * powerRate);
	this->Particles->SetFloatParameter(TEXT("Power"), powerRate);
	this->UpdateMesh();
}

float AAFire::GetPowerRate()
{
	return this->FireComponent->GetPowerRate();
}

void AAFire::Feed(float value)
{
	this->FireComponent->Feed(value);
	this->AtFeed();
}

void AAFire::UpdateMesh()
{
	if (this->BonfireMeshes.Num() > 0) {
		auto index = FMath::Clamp(int32(this->BonfireMeshes.Num() * this->GetPowerRate()), 0, this->BonfireMeshes.Num() - 1);
		this->BonfireMesh->SetStaticMesh(this->BonfireMeshes[index]);
	}
}
