// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FireComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LD50_API UFireComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFireComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Fire)
	float MaxPower = 100;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Fire)
	float StartingPower = 100;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Fire)
	float CurrentPower;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Fire)
	float DecayingSpeedPPS = 1.5;

	UFUNCTION(BlueprintPure)
	float GetPowerRate();

	UFUNCTION(BlueprintCallable)
	void Feed(float value);
};
