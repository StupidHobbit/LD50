// Copyright Epic Games, Inc. All Rights Reserved.

#include "LD50GameMode.h"
#include "LD50PlayerController.h"
#include "LD50Character.h"
#include "UObject/ConstructorHelpers.h"

ALD50GameMode::ALD50GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ALD50PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}