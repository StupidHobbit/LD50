// Fill out your copyright notice in the Description page of Project Settings.


#include "FireComponent.h"

// Sets default values for this component's properties
UFireComponent::UFireComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	this->CurrentPower = this->StartingPower - 0.01;
}


// Called when the game starts
void UFireComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	this->CurrentPower = this->StartingPower - 0.01;
}


// Called every frame
void UFireComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	this->CurrentPower -= this->DecayingSpeedPPS * DeltaTime;

	if (this->CurrentPower <= 0) {
		this->CurrentPower = 0;
	}
}

float UFireComponent::GetPowerRate()
{
	return this->CurrentPower / this->MaxPower;
}

void UFireComponent::Feed(float value)
{
	this->CurrentPower += value;
	if (this->CurrentPower > this->MaxPower) {
		this->CurrentPower = this->MaxPower;
	}
}
