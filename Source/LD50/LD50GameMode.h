// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LD50GameMode.generated.h"

UCLASS(minimalapi)
class ALD50GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALD50GameMode();
};



